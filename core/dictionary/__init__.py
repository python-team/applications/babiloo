#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia capiscuas@gmail.com
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
core package
'''

from dictionary import DictFormatError
from SlowoDictionary import SlowoDictionary
#from psdict import SDictionary
from sdictem import SDictionary
from pystardict import StarDict
import traceback

def Dictionary(dict_path):
    print "Loading %s ... "  % dict_path
    try:
        dict = StarDict(dict_path)
        print "(Stardict) type detected"
        return dict
    except DictFormatError, e:
        print e
        pass

    try:
        dict =  SDictionary(dict_path)
        print "(SDictionary) type detected"
        return dict
    except DictFormatError, e:
        #print e
        pass

    try:
        dict = SlowoDictionary(dict_path)
        print "(SlowoDictionary) type detected"
        return dict
    except DictFormatError, e:
        #print e
        pass

    print "No type detected"
    return None

