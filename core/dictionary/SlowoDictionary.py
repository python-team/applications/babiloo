#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __future__ import with_statement  # not available on S60
from dictionary import DictionaryType, Definition, DictFormatError

import bisect # not available on S60

class SlowoDictionary(DictionaryType):

    def __init__(self, filePath):
        DictionaryType.__init__(self, filePath)

    def __repr__(self):
        return "SlowoDictionary (%d definitions)" % len(self._DictionaryType__dict)

    def load(self, filePath):
        """Load dictionary into memory"""
        if not filePath.endswith('.dwa'):
            raise DictFormatError, "Not a valid slowo dictionary"
        self.filePath = filePath
        self.units = []
        with open(filePath) as fd:
            for line in fd:
                content = ''
                try:
                    line = unicode(line, self.encoding)
                except Exception, e:
                    raise Exception, "Unable to encode text in %s" % self.encoding
                word, end = line.split('=')
                word = word.strip()
                chunks = end.split(';')
                for chunk in chunks:
                    chunk = chunk.strip()
                    if not chunk:
                        continue
                    try:
                        trans, comment = chunk.split('//')
                    except:
                        trans = chunk
                        comment = None
                    trans = trans.strip()
                    if comment:
                        comment = comment.strip()
                    content += "%s //%s<br>" % (trans ,  comment)

                #self._Dictionary__keys.append(word)
                bisect.insort_left(self._DictionaryType__keys, word)
#                try:
#                    self._DictionaryType__dict[word].append(Definition(content))
#                except:
                self._DictionaryType__dict[word] = Definition(content)

    def save(self, filePath=None):
        """Write data to disk"""
        if not filePath:
            filePath = self.filePath
        with open(filePath, 'w') as fd:
            for word in self._Dictionary__keys:
                outstr = "%s = " % word
                chunks = []
                for definition in self._DictionaryType__dict[word]:
                    chunks = definition._Definition__content.split('\n')
                outstr += u' ; '.join(chunks)
                outstr = outstr.encode(self.encoding)
                print >> fd, outstr

    def getTypeStr(self):
        return "Slowo"
