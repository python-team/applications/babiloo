#
# Babiloo
# Copyright (c) 2008-2010 Ivan Garcia <capiscuas@gmail.com>

"""
Unit tests for dictionary.py
"""

import unittest
import os
import sys

sys.path.append('..')

from core.dictionary import DictionaryType
from core.psdict import SDictionary
from core import *

class TestDictionary(unittest.TestCase):
    """Dictionary test"""

    def test_loadPlainDictionary(self):
        """Dictionary should load dictionary from file"""

        test_dict = DictionaryType()
        self.assertEquals(len(test_dict), 0)

        sample_dict = Dictionary("data/sampledict.dwa")
        self.assertEquals(len(sample_dict), 7)

        self.assert_(isinstance(sample_dict, SlowoDictionary))
        #self.assertEquals(sample_dict.getParser() == DWAParser)

    def test_loadSDictionary(self):
        """Dictionary should load dictionary from file"""
        #See this http://sdict.com/en/format.php
        #and get the code from the sdiqt source in our babiloo bzr
        sdict = Dictionary("data/Catalan_English.dct")
        self.assertEquals(len(sdict), 1040)
        self.assert_(sdict['ulleres'] != None)       
        self.assert_(sdict[0] != None)

        self.assert_(type(sdict) == SDictionary)

        #self.assertEquals(sdict.getParser() == PlainTextParser)
        self.assertEquals(sdict.getVersion(),"1.0")
        self.assertEquals(sdict.getName(),"Catalan-English dictionary")

    def test_loadStarDictDictionary(self):
        """Dictionary should load dictionary from file"""

        sdict = Dictionary("data/stardict-qu-BO_es-1.0.zip")
        #self.assertEquals(len(sdict), 1040)
        #self.assert_(sample_dict['ulleres'] != None)       
        #self.assert_(sample_dict[0] != None)

        self.assertEquals(type(sdict) == StarDictDictionary)

        #self.assertEquals(sdict.getParser() == PlainTextParser)
        #self.assertEquals(sdict.getVersion() == "1.0")
        #self.assertEquals(sdict.getName() == "Catalan-English dictionary")

    def test_save(self):
        """Dictionary should correctly write dictionary to disk"""

        sample_dict = Dictionary("data/sampledict.dwa")
        self.assertEquals(len(sample_dict), 7)
        sample_dict.save("data/__output.dwa")
        sample2_dict = Dictionary("data/__output.dwa")
        self.assertEquals(len(sample2_dict), len(sample_dict))

        os.remove("data/__output.dwa")


    def test_getArticle(self):
        """getUnit() should return desired Translation object"""

        sample_dict = Dictionary("data/sampledict.dwa")
        self.assert_(sample_dict['du'] != None)       
        self.assert_(sample_dict[0] == 'vienas')

#    def test_addUnit(self):
#        """addUnit() should add new translation unit or update old one"""
#
#        editor = dicteditor.Editor()
#        editor.load("data/sampledict.dwa")
#        oldCount = len(editor.getUnits())
#
#        newUnit = dicteditor.Translation()
#        newUnit.setWord("test")
#        newUnit.addTranslation("utiutiu")
#
#        editor.addUnit(newUnit)
#
#        self.assertEquals(len(editor.getUnits()), oldCount + 1)
#
#
#    def test_removeUnit(self):
#        """removeUnit() should add new translation unit or update old one"""
#
#        editor = dicteditor.Editor()
#        editor.load("data/sampledict.dwa")
#        oldCount = len(editor.getUnits())
#
#        unit = editor.getUnit('vienas')
#        editor.removeUnit(unit)
#       
#        self.assertEquals(len(editor.getUnits()), oldCount - 1)
#
#
#    def test_unicode(self):
#        """All the strings must be unicode objects"""
#
#        editor = dicteditor.Editor()
#        editor.load("data/sampledict.dwa")
#
#        for unit in editor.getUnits()[:10]:
#            for trans in unit.getTranslations().keys():
#                self.assertEquals(type(trans), type(u''))
#       

if __name__ == "__main__":
    unittest.main()
