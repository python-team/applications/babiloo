#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

programFolder = None
import sys, os, platform

if os.path.isdir(sys.path[0]): #for Linux is /program_folder/           
    programFolder =  sys.path[0]
else: #for Windows is the /program_folder/run.py
    programFolder =  os.path.dirname(sys.path[0])

if platform.system() == "Windows":
        sys.stderr = open(os.path.join(programFolder,"babiloo.log"), "w") #The EXE file in windows will think that outputs here are errors, and it will show annoying mesage about run.exe.log

import logging
from optparse import OptionParser
import core.modules.configuration as conf

#
# Initial path
#sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
#

parser = OptionParser(description=conf.General.description, version=conf.General.version, option_list=conf.Terminal.option_list)
(options, args) = parser.parse_args()

if options.mode == 'gui':
    import qt.main
#elif options.mode == 'cli':
#    import cli.main

logging.basicConfig(level=options.logging,
                    format=conf.Logging.log_format,
                    datefmt='%y-%m-%d %H:%M',
                    #uncomment next two lines if we want logging into a file
                    filename=conf.Logging.log_path,
                    filemode=conf.Logging.log_mode,
                    )

# add a console logging handler if verbosity is turned on
if options.verbose:
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(options.logging)
    # set a format which is simpler for console use
    if options.output == "nerd":
        formatter = logging.Formatter("%(levelname)s::%(name)s # %(message)s")
    elif options.output == "human":
        formatter = logging.Formatter("%(message)s")
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)
# create a logger named 'babiloo.run'
# consequent ones should follow its parent as. 'babiloo.package.foo'
log = logging.getLogger("run")

if __name__ == "__main__":
    log.info('Babiloo started.')

    if options.mode == 'gui':
        qt.main.main(options)

    log.info('Babiloo ended.')
