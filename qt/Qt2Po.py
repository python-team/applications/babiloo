#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2008-2010 Ivan Garcia <contact@ivangarcia.org>
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import sys

import re
text = re.sub('QtGui\.QApplication\.translate\(\".*?\",\s(.*),\sNone,\sQtGui\.QApplication\.UnicodeUTF8\)',r'_(\1)',sys.stdin.read())

ignore = ['#000000','%','B','I','U','...','Alt','Shift','Win','Control','Babiloo']
for w in ignore:
        text = text.replace('_("%s")' %w, '"%s"' %w)

print text
