��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   W     a  	   v  
   �     �     �     �  	   �     �     �     �     �  	   �     �  �        �     �     �     �  	   �  	   �     �     �     �               #  
   )  $   4     Y  &   b     �     �     �     �     �     �     �  )        7     Q     ^  	   e     o     �     �     �     �     �  d   �  /   2     b     j     w     �     �     �     �     �     �     �  	             .     7     D     b     y     �  $   �     �     �     �     �  '   �       )   +     U     ]     n     }     �  (   �     �     �                     5     G     f      �     �  (   �  %   �     
       #   (  M   L  !   �  *   �     �  �   �     �  O        X     h     v     �     �     �  $   �  
   �     �      �     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-03-06 06:27+0000
Last-Translator: Marcos Lans <Unknown>
Language-Team: Galician <marcoslansgarza@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  segundos %s non se cargou: %s &Cancelar &Acerca de &Sobre Babiloo &Pechar &Configurar Babiloo &Ficheiro &Axuda &Acordo de licenza &Saír &Gardar no ficheiro &Procurar &Configuración Un nova versión de Babiloo foi liberada.

Nova versión: %s
Versión actual: %s

Quere descargar a nova versión neste momento? A&utores Abreviación Acerca de Babiloo Alerta Aparencia Aparencia Artigos Completar automaticamente Configuración de Babiloo Fondo Comportamento Negra Navegar... A construír o diálogo principal... Cancelar Diferenciar maiúsculas de minúsculas Limpar Limpar a caixa de procuras Clic para seleccionar a cor Altura predefinida Largura predefinida Dicionarios Nome do dicionario Non foi posible instalar o dicionario:
%s Cartafol dos dicionarios: Non esconder Abaixo Descargar Descargar dicionarios Descargar dicionario A descargar A descargar ficheiros... Elemento Erro Produciuse un erro durante a carga do dicionario.
O ficheiro está derramado ou non está soportado. Produciuse un erro mentres cargaba a palabra %s Exemplo Explicación Exportar o dicionario como... Fonte Primeiro plano Desde unha ligazón: Desde a listaxe: Configuración global Ir ao artigo segui&nte Ir ao a&rtigo anterior Historial Ignorar acentos Importar Información Instalar desde un ficheiro... Dicionarios instalados Idioma da interfaz Cursiva O enderezo da ligazón está baleiro A cargar %s... Xestionar os dicionarios... Baixar Subir Encontrouse unha versión máis recente Abrir un cartafol Por favor, primeiro cargue un dicionario. Plugins Ventá emerxente Pre-visualizar Imprimir o ar&tigo Pronunciar a palabra Pronunciar palabras usando este comando: Comunicar un problema A re-escanear dicionarios... Escanear a selección Procurar Seleccionar elemento Seleccionar fonte Seleccionar o tamaño da fonte Información rápida de uso: Amosar se non se atopa a palabra Amosar a  información Amosar a información sobre o dicionario Amosar se o modificador está premido Tamaño Reproducir a &palabra A iniciar a descarga (%s bytes) ... É necesario reiniciar o programa para que o novo idioma poida ser utilizado. Non se atopou a palabra <b>%s</b> Tempo antes de esconder o punteiro do rato Título Para instalar un novo dicionario desde Internet, seleccione <b>Xestionar os dicionarios...</b> e escolla un desde a pestana <b>Descargar dicionarios</b> Para instalar un novo dicionario desde un ficheiro, seleccione <b>Instalar desde un ficheiro...</b> no menú <b>Dicionarios</b> Para comezar a utilizar un dicionario, seleccióneo no menú <b>Dicionarios</b> Entradas totais Transcrición Traducir esta aplicación... Traducir esta aplicación... Transparencia Tipo Non se puido descargar o ficheiro %s Subliñado Arriba Visitar a páxina da aplicación 