��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   @     J  	   b     l     x     �     �     �     �     �  
   �     �     �     �  z   �     z  	   �     �     �  	   �  	   �  	   �     �     �  
   �     �     �             	   /     9     M     T     c          �     �     �  3   �     �          !     (     1     H     _  )   t     �     �      �  #   �  	   �     �       
     	   *  	   4  
   >     I     ^     w     �     �  
   �  
   �     �     �     �               ,     <     U     f     w     �     �     �     �     �     �     �  $        )  (   A     j     {     �     �     �     �      �     �       0   *     [     c  )   u  P   �  '   �  '        @  �   F  }   �  [   d     �     �     �     �          #     )     H     U     \     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-01-13 05:39+0000
Last-Translator: Heureka <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  seconden %s was niet geladen: %s &Afbreken &Informatie &Over Babiloo &Sluiten Babiloo &instellen &Bestand &Help &Licentieovereenkomst &Afsluiten Naar &bestand opslaan &Scannen &Instellingen Een nieuwe versie van Babiloo is uitgegeven.

Nieuwe versie: %s
Huidige versie: %s

Wilt u de nieuwe versie nu downloaden? Aut&eurs Afkorting Over Babiloo Waarschuwing Uiterlijk Uiterlijk Artikelen AutoAanvullen Instellingen van Babiloo Achtergond Gedrag Vet Bladeren... Opbouwen van het hoofdvenster... Annuleren Hoofdlettergevoelig Wissen Zoekvak wissen Klik om kleur te selecteren Standaardhoogte Standaardbreedte Woordenboeken Woordenboeknaam Woordenboek dat niet geïnstalleerd kon worden:
 %s Map die de woordenboeken bevat: Niet verbergen Omlaag Download Download woordenboeken Woordenboek downloaden Bezig met downloaden Bezig met het downloaden van bestanden... Element Fout Fout tijdens openen woordenboek. Fout bij het laden van het woord %s Voorbeeld Uitleg Woordenboek exporteren als... Lettertype Voorgrond Van link: Van lijst: Globale instellingen Ga naar v&olgend artikel Ga naar &vorig artikel Zoekgeschiedenis Accenten negeren Importeren Informatie Installeer van bestand... Geïnstalleerde woordenboeken Taal van programma: Cursief Linkadres is leeg Laden van %s... Woordenboeken beheren... Verplaats omlaag Verplaats omhoog Nieuwe versie gevonden Open map Open eerst een woordenboek. Plug-ins Pop-upvenster Voorbeeldweergave Artikel &afdrukken Het woord uitspreken Spreek woorden uit met dit commando: Rapporteer een probleem Woordenboeken opnieuw aan het scannen... Selectie scannen Zoeken Selecteer element Selecteer lettertype Kies lettertype-grootte Korte gebruikersinformatie: Tonen indien woord niet gevonden Toon informatie Toon woordenboek-informatie Alleen tonen indien modificeringstoets ingedrukt Grootte Woord &uitspreken Starten van het downloaden (%s bytes) ... De nieuwe taal zal zichtbaar zijn nadat u het programma opnieuw opgestart heeft. Het woord <b>%s</b> werd niet gevonden. Timeout voor het verbergen na muis-over Titel Om een woordenboek van het internet te installeren, selecteer <b>Woordenboeken beheren...</b> en kies er één uit het <b>Woordenboeken downloaden</b>-tabblad. Om een nieuw woordenboek van bestand te installeren, kies <b>Installeer van bestand...</b> uit het <b>Woordenboeken</b>-menu. Om een woordenboek te gaan gebruiken, selecteer er één uit het <b>Woordenboeken</b>-menu. Aantal artikelen Transcriptie Deze toepassing vertalen... Vertaal deze Toepassing... Transparantie Soort Kan bestand %s niet downloaden Onderstrepen Omhoog Homepage bezoeken 