��    l      |  �   �      0	     1	     :	     A	     H	     W	     ^	     q	     w	     }	     �	     �	     �	  	   �	     �	     �	     �	     �	  
   �	  	   �	     �	     �	     	
  
   
     %
     .
  	   3
     =
     U
     \
     k
     q
     �
     �
     �
     �
     �
  *   �
  
                       0     <     Q     Y  E   _     �     �     �     �  
   �     �     �               %     4     ;     @     U     l     �     �     �     �  	   �     �     �     �     �                -     5     D  #   W     {     �     �     �     �     �     �     �       	     !   #     E     c     h  $   t  @   �      �  $   �        �   &  l   �  H        f     u     �     �     �     �  	   �     �     �  �  �  	   �     �     �     �     �     �     �     �     �     �     �     �     �     	       
   !     ,     2     ;     D     M     ]  
   s  
   ~     �     �     �     �  '   �     �  	   �     �     	       	   '     1  &   A  
   h     s     w     �  
   �     �     �     �  F   �       
        !     <  
   A     L     b     z     �     �     �     �     �     �     �     �               &  	   9  	   C     M     a     m     �     �  	   �     �     �  )   �     �       	        '     ,  	   9     C     W     k     �     �  "   �  
   �     �      �  G        L      h     �  t   �  g     @   m     �     �     �     �     �     �  
             "     O       8   L   	          E      _         $   D   9   A       I          P       =   d   \   c       i   K   ?   )   ^       #   1   <   e   +      J   h              M   "                           F   !       U   b                  6       2           ;       4   0                    N   @          (           V   Y   W   7       3       j   Q      [      ,                     S       ]               C   H   %   l      B   *   f      5      &   `   -                  Z      g   G   
   >       /   T      :   k      R       '   X   .           a            seconds &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Don't hide Down Download Download Dictionaries Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-07-18 14:28+0000
Last-Translator: Westgaard <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekunder &Avbryt &Om &Om Babiloo &Lukk &Konfigurer Babiloo &Fil &Hjelp &Lisensavtale &Lukk &Lagre på fil &Scan &Innstillinger F%orfattere Forkortelse Om Babiloo Alert Utseende Utseende Artikler Autofullføring Babiloo Innstillinger Bakgrunnen Oppførsel Fet Bla gjennom... Bygger hoveddialog... Avbryt Skiller mellom store og små bokstaver. Tøm Tøm søk Klikk for å velge farge Default høyde Default bredde Ordbøker Navn på ordbok Ordbok som ikke kunne installeres:
 %s Ikke skjul Ned Last ned Last ned Ordbøker Laster ned Laster ned filer... Element Feilmelding Feil ved lastingen av ordboken.
Filen er ødelagt eller ikke støttet. Eksempel Forklaring Eksportér ordboken som... Font Forgrunnen Globale innstillinger Gå til %neste artikkel Gå til &forrige artikkel Historie Ignorer aksenter Importer Info Installer fra fil... Installerte Ordbøker Grensesnitt språk: Kursiv Link-referansen er tom Laster %s... Ordne Ordbøker... Flytt ned Flytt opp Ny versjon oppdaget Åpne mappe Last ned en ordbok først. Plugins Popup vindu Se først Prin&t artikkel Uttal ordet Uttal ord ved å benytte denne kommandoen Fortell oss om et Problem Omsøk av ordbøker... Søk valg Søk Velg element Velg font Velg fontstørrelse Kort Bruksanvisning Vis om ord ikke finnes Vis informasjon Vis informasjon om ordbok. Vis bare om modifiserer er trykket Størrelse Si &ord Starter nedlasting (%s bytes)... Det nye språket vil bli vist etter at programmet et startet opp igjen. Ordet <b>%s</b>finnes ikke. Pause før skjul etter mouseover Tittel For å installere en ny ordbok fra internett, velg: <b>Ordne Ordbøker...</b> og velg fra <b>Last ned Ordbøker<b> . For å installere en ny ordbok fra en fil, velg<b>installer fra fil...</b> fra <b>Ordbøker</b> -menyen For å begynne å bruke ordboken, velg fra <b>Ordbøker</b>meny. Totalt artikler Transkripsjon Oversett denne Applikasjonen Åpenhet Tast Kunne ikke laste ned fil %s Understrek Opp Besøk Hjemmesiden 