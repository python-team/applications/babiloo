��    s      �  �   L      �	     �	     �	     �	     �	     �	     �	     
     
     
     #
     6
     <
     J
  	   P
  �   Z
     �
     �
     �
        
     	             $     1  
   B     M     V  	   [     e     }     �     �     �     �     �     �     �     �  *   �  #   )  
   M     X     ]     f     |     �     �     �     �  E   �          %     -     9     Q  
   V  
   a  
   l     w     �     �     �     �     �     �     �     �                    3     A  	   X     b     j          �     �     �     �     �     �  #   �          "     =     L     S     b     n          �  	   �  !   �     �     �  $   �  @         R  $   s     �  �   �  l   (  H   �     �     �     �          7     D     I  	   d     n     q  �  �  	        %  
   ;     F     O     c     j  	   ~     �     �     �     �     �  
   �  m   �  	   <     F     N     a  
   j     u  
   }     �     �     �     �  	   �     �     �     �     �  	             0     J     X  
   e     p      �     �     �     �     �     �     �     �       	        $  J   *     u  	   �     �     �     �     �  
   �  	   �     �     �          <     E     [     _     f     |     �     �     �     �     �     �     �          !  #   -     Q     `  
   m     x  
   �     �     �  #   �     �     �               #     7     K     h     u     �     �  #   �  G   �          ,     C  �   K  l   �  O   J     �     �     �     �     �               )     5     <     f   '          F   K          e              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   l   g              n            A       Z   	       Y   M   )          ,   R   r   j          5            <   >   m           i   L   *   .       q           #   
      7   2      b   \      4       !             (   h   6              s   -      S   o           `       "   V       &       :       c   N   %       =          9      /              ;   a   X   O   [   _   W      k   +   3           E   Q             d       1   p   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-06-27 09:28+0000
Last-Translator: Jiri Grönroos <Unknown>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  sekuntia %s :aa ei ladattu: %s &Keskeytä &Tietoja &Tietoja Babiloosta &Sulje &Konfiguroi Babiloo &Tiedosto &Ohje &Lisenssisopimus &Lopeta &Tallenna nimellä &Skannaa &Asetukset Uusi versio Babiloosta on julkaistu.

Uusi versio: %s
Nykyinen versio; %s

Haluatko ladata uuden version nyt? &Tekijät Lyhenne Tietoja Babiloosta Hälytys Ulkonäkö Ulkoasu Artikkelit Automaattinen täyttö Babiloon asetukset Tausta Käyttäytyminen Lihavoitu Selaa... Rakennetaan päädialogia Peru Huomioi kirjainkoko Tyhjennä Tyhjennä hakukenttä Klikkaa valitaksesi väri Oletuskorkeus Oletusleveys Sanakirjat Sanakirjan nimi Sanakirjaa ei voitu asentaa:
 %s Sanakirjojen luettelointi: Älä piilota loppuun Lataa Lataa sanakirjoja Lataa sanakirja Ladataan Ladataan tiedostoja... Elementti Virhe Virhe ladatessa sanakirjaa.
Tiedosto on korruptoitunut tai sitä ei tueta. Virhe ladattaessa sanaa %s Esimerkki Selitys Vie sanakirja tiedostoon... Fontti Etuala Linkistä: Listasta: Sivuston laajuiset asetukset Siirry &seuraavaan artikkeliin Siirry &edelliseen artikkeliin Historia Ohita aksenttivirheet Tuo Tietoa Asenna tiedostosta... Asennetut sanakirjat Käyttöliittymän kieli: Kursiivi Osoite on tyhjä Ladataan tiedostoa %s... Hallitse sanakirjoja Siirry alas Siirry ylös Uusi versio havaittu Avaa kansio Ole hyvä ja lataa sanakirja ensin. Liitännäiset Popup-ikkuna Esikatselu &Tulosta artikkeli Lausu sana Lausu sanat tällä komennolla: Raportoi ongelmasta Uudelleen etsitään sanakirjoja... Skannausvalinta Hae Valitse elementti Valitse fontti Valitse fontin koko Lyhyt käyttöohje: Näytä, jos sanaa ei löydy näytä info Näytä tietoa sanakirjasta Koko Äännä &sana Aloitetaan lataaminen (%s tavua...) Uutta kieltä käytetään ohjelman uudelleenkäynnistämisen jälkeen. Sanaa <b>%s</b> ei löydy. Piilota hiiren osoitin Otsikko Asentaaksesi uuden sanakirjan internetistä, valitse <b>Hallitse sanakirjoja...</b> ja valitse sanakirja <b>Lataa sanakirjoja</b> välilehdestä. Asentaaksesi uuden sanakirjan tiedostosta, valitse <b>Asenna tiedostosta...</b> <b>Sanakirjat</b>-valikosta. Aloittaaksesi sanakirjan käyttämisen, valitse se <b>Sanakirjat</b>-valikosta. Artikkeleita yhteensä Transkriptio Käännä tämä ohjelma... Käännä tämä ohjelma... Läpinäkyvyys Tyyppi Tiedostoa %s ei voida ladata Alleviivaus Alkuun Vieraile kotisivulla 