��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �     O     V     r     ~     �     �     �  
   �     �     �     �                  �   -     �     �     �               $     6     ;     U     i     o     {  
   �  -   �  
   �  &   �     �     �          !     ;     W     d  *   v     �     �     �  
   �     �     �            
   /     :     @     ]     y     �  )   �     �  
   �     �     �     �          $     >     K     a  
   h     s     �     �     �     �     �     �          +     <     \  *   l     �     �     �     �     �  *   �          '     C     V     b     z  "   �     �  *   �     �  )     *   :  
   e     p  %   |  O   �  !   �  3        H  �   T  �   �  Y   �     �  
   �               9     F     M     i     x     }     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-06-08 13:44+0000
Last-Translator: huanctv <huanctv@gmail.com>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  giây %s chưa được nạp: %s &Hủy bỏ &Về chương trình &Thông tin về Babiloo Đóng &Thiết lập Babiloo &Tập tin &Trợ giúp &Thoả thuận &Thoát Lưu vào tập tin &Quét &Cài đặt Một phiên bản mới của Babiloo đã được phát hành.

Phiên bản mới: %s
Phiên bản cũ: %s

Bạn có muốn tải phiên bản mới bây giờ không? Các tác giả Viết tắt Về Babiloo Cảnh báo Sự hiển thị Sự hiển thị Bài Tư động hoàn chỉnh Cài đặt Babiloo Nền Chế độ Chữ đậm Duyệt... Đang xây dựng đối thoại chính... Hủy bỏ Để ý chữ hoa và chữ thường Xóa Xóa khung tìm kiếm Bấm chọn màu Chiều cao mặc định Chiều rông mặc định Từ điển Tên từ điển Từ điển không thể cài đặt:
 %s Thư mục lưu từ điển Không ẩn Xuống Tải về Tải từ điển về Tải từ điển Đang tải Đang tải dữ liệu... Yếu tố Lỗi Lỗi khi tải từ điển Có lỗi khi nạp từ %s Ví dụ Giải thích Xuất từ điển dưới dạng... Kiểu chữ Màu sắc Từ liên kết: Từ danh sách: Cài đặt toàn cầu Vào mục kế tiếp Trở về mục trước Lịch sử Từ chối âm dấu Nhập Thông tin Cài đặt từ tập tin Từ điển đã tải Ngôn ngữ chung Chữ nghiêng Trường địa chỉ rỗng Đang tải %s... Quản lý từ  điển Di chuyển xuống Di chuyển lên Phát hiện Phiên bản Mới Mở Thư mục Vui lòng tải từ điển về trước Phần mềm nhúng Cửa sổ hội thoại Xem trước In bài luận Đọc từ Đọc từ sử dụng mệnh lệnh này Báo cáo lỗi Quét lại từ điển... Quét lựa chọn Tìm kiếm Lựa chọn yếu tố Chọn kiểu chữ Chọn kích thước kiểu chữ Hướng dẫn sơ lược Hiển thị nếu từ không tìm thấy Hiển thi thông tin Hiển thị thông tin về từ điển Chỉ hiện nếu nhấn phím bổ trợ Kích cỡ Đọc từ Bắt đầu tải về (%s byte) ... Ngôn ngữ mới sẽ hiện lên sau khi khởi động lại chương trình Từ <b>%s</b> không tìm thấy Thời gian chờ để ẩn khi có chuột di qua Tiêu đề Để cài đặt một từ điển mới từ mạng, chọn <b>Quản lý từ điển...</b> và chọn một trong thanh <b>Tải từ điển</b> Để cài đặt một từ điển mới từ tập tin, chọn <b>Tải từ tập tin...</b> từ mục lục <b>Từ điển</b> Để bắt đầu sử dụng từ điển, chọn từ mục lục <b>Từ điển</b> Tất cả bài luận Phiên âm Dịch Chương Trình Này Dịch ứng dụng này... Trong suốt Dạng Không thể tải tệp %s Gạch dưới Lên Thăm trang nhà 