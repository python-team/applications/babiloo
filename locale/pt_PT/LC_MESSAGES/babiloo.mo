��    v      �  �   |      �	     �	     �	     
     
     
     -
     4
     G
     M
     S
     f
     l
     z
  	   �
  �   �
               "     0  
   6  	   A     K     T     a  
   r     }     �  	   �     �     �     �     �     �     �     �     
       &   '  )   N  *   x  #   �  
   �     �     �     �     �     
          +     3  E   9          �     �     �     �  
   �  
   �  
   �     �               -     5     D     K     k     p     �     �     �     �     �     �  	   �     �               (     H     P     ]     e     t  #   �     �     �     �     �     �     �               2  	   I  !   S     u     �     �  $   �  @   �      
  $   +     P  �   V  l   �  H   M     �     �     �     �     �     �       	        &     )  �  8  	   �     �     �                    '  	   ;     E     L     `     g  	   ~     �  }   �               )     ;  
   A  
   L     W     _     y     �     �     �     �     �     �     �               =     P     d     q  ,   �  /   �  /   �  '        :     H  
   N     Y     q     �     �     �     �  M   �          )     1     >     [     i     x     �     �     �     �  
   �     �     	           3  !   @     b     z     �     �     �     �     �     �     �       *     
   F     Q     ^     n       .   �     �     �     �  	             +     =  %   W  %   }     �  )   �  )   �               (  J   D  (   �  0   �     �  �   �  t     H   �     =     N     \     y     �     �  $   �  
   �     �     �     i   &          H   M         h              `           '   W   V   F       J   @   C   K       E      R   9       I   1                  L   o   j              q            B       \   	       [   O   *          -   T   u   m          6            =   ?   p           l   N   +   /       >           "   
      8   3      e   ^      5                     )   k   7              t   .      U   r           c       !   X       %       ;       f   P   $       (          :      0              <   d   Z   Q   ]   a   Y      n   ,   4           G   S   b         g       2   s   _   D   A   #   v     seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary successfully installed:
 %s Dictionary that coudn't be installed:
 %s Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Import one or many dictionaries Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-04-27 03:46+0700
PO-Revision-Date: 2009-04-27 18:51+0000
Last-Translator: Marco Rodrigues <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2009-04-27 18:56+0000
X-Generator: Launchpad (build Unknown)
  segundos %s não foi carregado: %s &Abortar &Acerca &Acerca do Babiloo Fe&char &Configurar Babiloo &Ficheiro &Ajuda Acordo de &Licença &Saír &Guardar para ficheiro &Examinar &Definições Uma nova versão do Babiloo foi lançada.

Nova versão: %s
Versão actual: %s

Gostaria de transferir agora a nova versão ? A&utores Abreviatura Acerca do Babiloo Aviso Aparência Aparência Artigos Completar Automaticamente Definições do Babiloo Plano de fundo Comportamento Negrito Procurar... A construir janela principal... Sensível à capitalização Limpar Limpar caixa de pesquisa Clique para seleccionar a cor Altura predefinida Largura predefinida Dicionários Nome do dicionário O dicionário foi instalado com sucesso:
 %s Não foi possível instalar o dicionário::
 %s Não foi possível instalar o dicionário::
 %s Pasta de armazenamento de dicionários: Não esconder Baixo Transferir Transferir Dicionários Transferir dicionário A transferir A transferir ficheiros... Elemento Erro Erro ao tentar carregar o dicionário.
Ficheiro corrompido ou não suportado. Erro ao carregar a palavra %s Exemplo Explicação Exportar dicionário como... Tipo de Letra Primeiro plano A partir do endereço: A partir da lista: Configurações globais Ir para o p&róximo artigo Ir para artigo &anterior Histórico Ignorar acentos Importar Importar um ou mais dicionários Informação Instalar ficheiro a partir de ... Dicionários Instalados Idioma da Interface: Itálico O endereço está vazio A carregar %s... Gerir Dicionários... Mover para baixo Mover para cima Nova versão detectada Abrir Pasta Por favor carregue o dicionário primeiro. Extensões Janela popup Pré-visualizar Im&primir artigo Pronunciar a palavra Pronuncie estas palavras utilizando o comando: Comunicar um problema A examinar os dicionários... Pesquisar selecção Pesquisar Seleccionar elemento Seleccionar fonte Seleccionar Tipo de Letra Informação rápida de utilização: Mostrar se nenhuma palavra encontrada Mostrar informação Mostrar informações sobre o dicionário Apenas mostrar se notificador pressionado Tamanho Reproduzir &palavra A transferir (%s bytes) ... É necessário reiniciar o programa para que o novo idioma seja utilizado. A palavra <b>%s</b> não foi encontrada. Tempo até esconder ponteiro se estiver por cima Título Para instalar um dicionário pela internet, seleccione <b>Gerir Dicionários...</b> e escolha um do separador <b>Transferir Dicionários</b>. Para instalar um dicionário de um ficheiro, seleccione <b>Instalar ficheiro de ...</b> do menu <b>Dicionários</b>. Para utilizar um dicionário, seleccione um no menu <b>Dicionários</b>. Total de artigos Transcrição Traduzir esta aplicação... Traduzir esta Aplicação Transparência Tipo Impossível transferir o ficheiro %s Sublinhado Cima Visitar Sítio Web 