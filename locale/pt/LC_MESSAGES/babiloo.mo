��    �      t  �   �      �     �     �     �     �     �                    %     +     >     D     R  	   X  �   b     �     �     �       
     	        #     ,     9  
   J     U     ^  	   c     m  	   }     �     �     �     �     �     �     �     �            *      "   K  *   n  #   �  
   �     �     �     �     �                !     )     1  E   7     }     �     �     �     �     �     �  
   �     �  
   �  
   �                    2     J     P     h     p          �     �     �     �     �     �     �     �          
           .  
   <     G  	   ^     h     p  	   �     �     �     �     �     �  
   �     �     �     �  #        /     @     [     c     r     y     �     �     �     �  	   �  !   �               $     ,  $   8     ]  @   e      �  $   �     �  �   �  l   |  H   �     2     A     O     m     �     �     �  	   �     �     �  �  �  	   s     }     �     �     �     �     �     �     �     �     �     �  
        !  }   1     �     �     �     �  
   �  
   �     �     �          (     7     E  
   M     X     q     }     �     �     �     �     �               (     5  0   I  #   z  /   �  '   �     �       
   
          )     @     S     g     p     x  M   }     �  	   �     �     �       
   %     0     >     M     V     m     �     �     �     �     �     �  
   �     �                9     F     c     {     �     �     �     �     �     �     �  
   �     �               -  
   D     O     R  *   ^  
   �     �  
   �     �     �     �  .   �          '     D     J  	   _     i     ~     �  %   �  %   �     �  )      )   5      _      g      p      �      �   J   �   (   �   0   !     O!  �   W!  t   �!  H   Z"     �"     �"     �"     �"     �"     #  %   #  
   3#     >#     C#     t   9   �   M          	   W      m       ?   w       y   a       �       =          k   v   �       b           p       F       _       -          �          o   #         0           1      ^   C   *         j       T   Q       N   @   �              B                 +      d       ,   h                        !       E       "   R   �           D   c   Y   %           K       �   .   H   �   L   ;       \   O      /              8              
   �   �   �                 V       G       J   |           :   >      P   q   7   `   (   5       )          ]       6   4   '       l   s   A   }   ~   {         f   S       U      i   g   e   X   x   u             r   n   [   3   �   2       I   z       <   &   Z       $     seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Brazilian British English Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary selected couldn't be installed. Dictionary successfully installed. Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element English Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Estonian Example Explanation Export dictionary as... Finnish Font Foreground French From link: From list: German Global settings Go to &next article Go to &previous article Greek Help us with a donation History Ignore accents Import Import one or more dictionaries Info Install from file... Installed Dictionaries Interface Language: Italian Italic Japanese Korean Link address is empty Loading %s... Luxembourgish Macedonian Manage Dictionaries... Move down Move up New Version Detected Norwegian Ok Open Directory Please load a dictionary first. Plugins Popup window Portuguese Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Russian Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Spanish Speak &word Starting the download (%s bytes) ... Swedish The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-09-21 18:25+0000
Last-Translator: Marco Rodrigues <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  segundos %s não foi carregado: %s &Interromper &Sobre Sobre Babiloo Fe&char &Configurar Babiloo &Arquivo &Ajuda Acordo de &Licença &Sair &Guardar para ficheiro Pe&squisar Configurações Uma nova versão do Babiloo foi lançada.

Nova versão: %s
Versão actual: %s

Gostaria de transferir agora a nova versão ? A&utores Abreviatura Sobre Babiloo Aviso Aparência Aparência Artigos Completar Automaticamente Definições do Babiloo Plano de fundo Comportamento Negrito Brasileiro Inglês da Grã-Bretanha Procurar... A construir janela principal... Cancelar Sensível à capitalização Limpar Limpar caixa de pesquisa Clique para seleccionar a cor Altura predefinida Largura predefinida Dicionários Nome do dicionário O Dicionário escolhido não pode ser instalado. Dicionários instalado com sucesso. Não foi possível instalar o dicionário::
 %s Pasta de armazenamento de dicionários: Não esconder Baixo Transferir Baixar Dicionários Transferir dicionário Fazendo o Download Transferir arquivos Elemento Inglês Erro Erro ao tentar carregar o dicionário.
Ficheiro corrompido ou não suportado. Erro ao carregar a palavra %s Estoniano Exemplo Explicação Exportar dicionário como... Finlandês Tipo de Letra Primeiro plano Francês A partir do endereço: A partir da lista: Alemão Configurações globais Ir para o p&róximo artigo Ir para artigo &anterior Grego Ajude-nos com um donativo Histórico Ignorar acentos Importar Importar um ou mais dicionários Informação Instalar a partir do arquivo Dicionários Instalados Idioma da Interface: Italiano Itálico Japonês Coreano O endereço está vazio A carregar %s... Luxemburguês Macedónio Gerenciar dicionários Mover para baixo Mover para cima Nova versão detectada Norueguês Ok Abrir Pasta Por favor carregue o dicionário primeiro. Extensões Janela popup Português Pré-visualizar Im&primir artigo Pronunciar a palavra Pronuncie estas palavras utilizando o comando: Comunicar um problema Reexaminando os dicionários Russo Pesquisar selecção Pesquisar Seleccionar elemento Seleccionar fonte Seleccionar Tipo de Letra Informação rápida de utilização: Mostrar se nenhuma palavra encontrada Mostrar informação Mostrar informações sobre o dicionário Apenas mostrar se notificador pressionado Tamanho Espanhol Reproduzir &palavra Iniciando o download (%s bytes) Sueco É necessário reiniciar o programa para que o novo idioma seja utilizado. A palavra <b>%s</b> não foi encontrada. Tempo até esconder ponteiro se estiver por cima Título Para instalar um dicionário pela internet, seleccione <b>Gerir Dicionários...</b> e escolha um do separador <b>Transferir Dicionários</b>. Para instalar um dicionário de um ficheiro, seleccione <b>Instalar ficheiro de ...</b> do menu <b>Dicionários</b>. Para utilizar um dicionário, seleccione um no menu <b>Dicionários</b>. Total de artigos Transcrição Traduzir esta aplicação... Traduzir esta Aplicação Transparência Tipo Não foi possível baixar o aquivo %s Sublinhado Cima Visitar HomePage 