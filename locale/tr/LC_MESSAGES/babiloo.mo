��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �     T     \     o  
   x     �     �     �     �     �     �  
   �     �     �     �  v     	   {  	   �     �     �  
   �  
   �  	   �  
   �     �     �  
   �     �  	      #   
     .     5     T     \     s     �     �     �     �     �  #   �          !     *     1     A     P     ]     u     {  v   �  %   �       
   $     /  	   M     W     _  	   n     x     �     �     �     �     �     �     �     �     
                7     I     `     p       	   �  $   �  
   �     �  	   �     �        ,        B      Q     r     �  
   �     �     �     �      �     �       #        B     H  !   Y  C   {  )   �  0   �       �   #  �   �  X   8     �     �     �     �     �     �     �                    g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-05-20 16:27+0000
Last-Translator: Angel Spy <melissadilara@yahoo.com>
Language-Team: Turkish <tr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  saniye %s yüklenmedi: %s &Vazgeç &Hakkında &Babiloo Hakkında &Kapat Babiloo'yu &Yapılandır &Dosya &Yardım &Lisans Anlaşması &Çıkış Dosyaya &kaydet &Tara &Ayarlar Babiloo'nun yeni sürümü tespit edildi.

Yeni Sürüm: %s
Mevcut Sürüm: %s

Yeni sürümü indirmek ister misiniz? Y&azarlar Kısaltma Babiloo Hakkında Uyarı Görünüm Görünüm Makaleler OtoTamamla Babiloo Ayarları Arkaplan Davranış Kalın Gözat... Ana diyalog yapılandırılıyor... İptal Büyük küçük harf duyarlı Temizle Arama kutusunu temizle Renk seçmek için tıklayın Varsayılan yükseklik Varsayılan genişlik Sözlükler Sözlük adı İndirilemeyen sözlük:
 %s Sözlüklerin depolandığı dizin: Gizleme Aşağı İndir Sözlük İndir Sözlük indir İndiriliyor Dosyalar indiriliyor... Öğe Hata Sözlük yüklemesi sırasında hata oluştu.
Dosya zarar görmüş ya da program tarafından desteklenmiyor olabilir. %s kelimesi yüklenirken hata oluştu Örnek Açıklama Sözlüğü farklı kaydet... Yazıtipi Önplan Bağlantıdan: Listeden: Genel ayarlar &Sonraki makaleye git &Önceki makaleye git Geçmiş Aksanları yoksay İçe aktar Bilgi Dosyadan yükle... Yüklenmiş Sözlükler Arayüz Dili: İtalik Bağlantı adresi boş %s yükleniyor... Sözlükleri Yönet... Aşağı taşı Yukarı taşı Yeni Sürüm Algılandı Dizin Aç Lütfen önce sözlüğü yükleyin. Eklentiler Açılır pencere Önizleme Makaleyi yazdı&r Kelimeyi telaffuz et Kelimeleri bu komutu kullanarak telaffuz et: Problem Bildir Sözlükler tekrar taranıyor... Tarama seçimi Ara Öğe seç Yazıtipi seç Yazıtipi boyutunu seçin Kullanım bilgisi özeti Eğer kelime bulunamazsa göster Bilgi göster Sözlük bilgilerini göster Hatırlatıcı basıldıysa göster Boyut &Kelimeyi söyle İndirme başlıyor (%s bayt) ... Yeni dil program yeniden başlatıldığında görüntülenecektir. <b>%s</b> şeklindeki kelime bulunamadı. Mouse hareketsizliğinden dolayı zaman aşımı Başlık İnternetten yeni bir sözlük yüklemek için <b>Sözlükleri Yönet...</b> bölümüne girip, <b>Sözlük İndir</b> alanından seçim yapınız. Dosyadan yeni bir sözlük yüklemek için <b>Sözlükler</b> menüsündeki <b>Dosyadan yükle...</b> seçeneğini kullanınız. Sözlüğü kullanmaya başlamak için <b>Sözlükler</b> menüsünden seçim yapınız. Tüm makaleler Uyarlama Bu Uygulamayı Çevir Bu Uygulamayı Çevir... Şeffaflık Tür %s dosyası indirilemiyor Altı Çizili Yukarı AnaSayfayı Ziyaret Et 