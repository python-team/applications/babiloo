��    i      d  �   �       	     	     
	      	     '	     .	     =	     D	     W	     ]	     c	     v	     |	     �	  	   �	  �   �	     
     %
     2
     @
  
   F
  	   Q
     [
     d
     q
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
       &     )   <  
   f     q     v     �     �     �     �     �     �     �     �       
     
     
   "     -     =     Q     i     q     x     �     �     �     �     �     �     �       	        )     1     F     f     n     {     �     �  #   �     �     �     �                    &     7     P  	   g  !   q     �     �  $   �  $   �  @   �      /     P  �   V  l   �  H   M     �     �     �     �     �  	   �     �     �  q       ~     �     �     �     �     �     �     	          !     =     I  	   c     m  �   {     *     <     L     h     u     �  
   �     �     �  
   �     �  &   �  
          9   4     n     �     �     �  <   �  :        L  
   Z     e     �     �     �     �  D   �  
     
     %   %     K  
   X     c     r       ,   �  &   �  
   �  
   �  0        4     E      a     �     �     �     �  "   �     �       #   $  F   H     �     �  
   �     �     �  >   �     '  ,   C     p     �     �     �     �  *   �  A   �     @  9   Z     �     �  /   �  <   �  ^     -   t  
   �  �   �  �   �  w   K     �     �     �       &   
     1     E     L     N       8   K   
   %      E       c      6   "   D   \   A   4   H                  =   b   Z   a       f   J   ?       ]      !   /   <       )       I   e              L          $                               -   `       @                  0           ;       2   .                    M   3   T      '           U       V   7       1   Y   g   P            *              O       R       [              C   G   #   i      B   (   :      5          ^   +                  X   	   d   F      >           S      9   h      Q       &   W   ,           _            seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Bold Building main dialog... Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary successfully installed:
 %s Dictionary that coudn't be installed:
 %s Don't hide Down Download Dictionaries Downloading Downloading files... Element Error Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Import Import one or many dictionaries Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Size Speak &word Starting the download (%s bytes) ... The dictionary is already installed. The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Translate This Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: Babiloo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-04-08 15:28+0100
PO-Revision-Date: 2009-04-09 18:31+0500
Last-Translator: Zabeeh Khan <zabeehkhan@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: /home/igarcia/workspace/babiloo/qt
  سېکنډونه %s ونه لېښل شوه: %s بندول& په اړه& د بېبيلو په اړه& بندول& بېبيلو سازول& دوتنه& مرسته& د منښتليک توکي& بندول& دوتنې کې ساتل& ځېرل& امستنې& د بېبيلو يوه نوې نسخه خوشې کړی شوې ده.

%s نوې نسخه:
%s اوسنۍ نسخه:

غواړﺉ چې اوس يې نوې نسخه رولېښﺉ؟ ليکوالان& لنډه بڼه د بېبيلو په اړه خبرتيا ښکارېدنه ښکارېدنه ليکنې خپله بشپړول د بېبيلو امستنې شاليد ډبل اره کړکۍ جوړول کيږي... پاکول لټون بکس پاکول د رنګ د ټاکلو دپاره دلته کېکاږﺉ تلواله اوږدوالی تلواله پلنوالی ويېپانګې د ويېپانګې نوم ويېپانګه په بريا سره ولګول شوه:
 %s هغه ويېپانګه چې ونه لګولی شوه:
 %s نه پټول لاندې ويېپانګې رالېښل لېښل کيږي دوتنې لېښل کيږي توکی تېروتنه ويې په لېښلو کې ستونزه رامنځته شوه %s د بېلګه سپړنه ويېپانګه بهرول لکه... ليکبڼه مخليد له تړنې: له لړه: ټولګړې امستنې &راتلونکې ليکنې ته ورتلل &مخکنۍ ليکنې ته ورتلل مخينه راوړل يوه يا ډېرې ويېپانګې راوړل خبرتياوې له دوتنې لګول... لګول شوې ويېپانګې د برسېر ژبه: کوږ د تړنې پته تشه ده ...لېښل کيږي ‎%s ويېپانګه سمبالول... لاندې خوځول پاس خوځول نوې نسخه وموندل شوه لورينه وکړﺉ لومړی يوه ويېپانګه ولېښﺉ. لګونونه بړبوکيزه کړکۍ مخکوت ليکنه چاپول& ويې وېل د دې بولۍ په کارولو سره ويېونه وېل: ستونزه راپورول ويېپانګې بيا ځېرل کيږي... ټاکنه لټول لټول توکی ټاکل ليکبڼه ټاکل د ليکبڼې کچ ټاکل د کارونې لنډې خبرتياوې: که چېرې ويې ونه موندل شوه، وويې ښييه خبرتياوې ښودل د ويېپانګې په اړه خبرتياوې ښودل کچ ويې &وېل  باېټه)... %sرالېښنه پېليږي ( دا ويېپانګه د مخکې نه لګول شوې ده. نوې ژبه به د کاريال د بيا پېلولو څخه وروسته وښودل شي. .ويې ونه موندل شوه ‎<b>%s</b> سرليک .ټوپ څخه يوه وټاکﺉ ‎<b>ويېپانګې رالېښل</b>‎ وټاکﺉ او بيا د‎ <b>ويېپانګې سمبالول...</b>‎ له انټرنېټ څخه د نوې ويېپانګې د رالېښلو لپاره .وټاکﺉ ‎<b>له دوتنې لګول</b>‎ غورنۍ څخه ‎<b>ويېپانګې</b>‎ له دوتنې څخه د نوې ويېپانګې د لګولو لپاره، د .غورنۍ څخه يوه وټاکﺉ‎ <b>ويېپانګې</b>‎ د ويېپانګې د کارولو لپاره، د ټولې ليکنې دا کاريال ژباړل روڼوالی ډول دوتنه نه شي رالېښلی %s لاندې کرښه پاس کورپاڼه کتل 