��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  
   n     y     �     �     �     �     �     �     �     �  	             -     >  �   K     !     2     @     T     ]     f     n     �     �     �     �     �     �     �       '     
   C     N     f     �     �     �     �  0   �               -  	   4     >     X  
   j     u  	   �  	   �     �  Q         r     z     �     �  
   �     �     �     �     �          3     G     `     o     x     �     �     �     �     �     �       
        (     <  N   N     �     �     �     �     �     �           *     K  
   ^     i     }     �     �  2   �             6   .     e     k  #   x  c   �  D      2   E     x  B  �    �  �   �      c!  	   |!  "   �!     �!     �!     �!     �!     �!     �!     �!     g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: Amos Batto <amosbatto@yahoo.com>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2009-06-06 05:09+0000
Last-Translator: amosbatto <amosbatto@yahoo.com>
Language-Team: Runasimipi.org <http://www.runasimipi.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  k'atanaka %s janiw q'ipxarutakiti: %s Jan &munaña &Aka tuqita &Babiloon yatiyawipa... &Jist'antaña &Babiloo qutu ajllita... &Kipu &Yanapa &Iyawsat kamich Babiloota &Mistuña &Kipur imaña &Thaqhat ullaña Ajllita&naka Mä machaq amuy babiloox qhanaruw mistu.
Machaq amuyu: %s
Jichha amuyu: %s

jichhax machaq amuy apaqañ munasmati?

Una nueva versión de Babiloo ha salido a la luz.
Le gustaría descargar la nueva versión ahora? &P'iqinchirinaka Jisk'aptayawi Babiloon yatiyawipa Yatiyawi &Uyñaqa Uyñaqa &Jach'a amuyunaka &Sapak phuqhachasiri Babiloon ajllt'atanakapa Sami qhipaxa Sarawinak uñacht'ayiri Ch'iyara &Yant'aw uñjaña... Nayrir sarantayañ wintana... Janiw saña &Jisk'a jach'a qillqanak sum uñakipawi Phiskhuña Aru thaqhañ phiskhuña Kliktayaña sami ajlliñataki Nayraqat ajllit ukch'a alaya Nayraqat ajllit anchu &Arupirwanaka Arupirwan sutipa Aka arupirwanakax janiraw achuntayañjakiti:
 %s Arupirwanak imañ tuqi: Jan imantaña Manqha Waraqaña Arupirwanak &waraqaña... Arupirw waraqaña Apaqaskiwa Kipunak q'ipnuqaña... Mä t'aqa Phantjata Pantjawi arupirwa q'ipiqañ yant'askasa.
Jan aski kipu.

Error intentando cargar el diccionario.
Archivo corrupto o no válido. "%s" aru apaqañkamax pantjasiwiw utjawayi.
Error mientras se cargaba la palabra. Sañani Yatxatayäwi Arupirw apsuña kunjama... Kastkast qilqanaka Sami uñta Chinthapit tuqita: Tantachat tuqita: &Taqi kun ajlliri &Qhipir jach'a amuyur saraña &Nayrir jach'a amuyur saraña I&mat aru thaqhaña Jan yaqkay qilqa &chimpu Jayar apayaña Yatiyawi &Kiputpach achuntayaña... A&rupirwanak achuntata Babiloon aru uñchawi: K'umjata Chinthapita utjawix ch'usakiwa %s apkataskiwa... Arupirwanak &apnaqaña... Maqaña Maqhataña Machaq amuy katjata Wayaq jist'araña Ampsum nayraqat mä arupirw q'ipiqam.
Por favor primero cargue un diccionario. &Yapt'awinaka &Wintanat yaqha wintan misturi Nayra uñjawi Jach'a amuy &liq'iña A&ru arsuña Aru sall amuy apnaqawi: Jan &walt'aw yatiyaña Wiltat arupirwanak ullakipawi... Ajllit u&llakipiri &Thaqhaña Mä t'aqat ajlliña Kast kast qilqanaka ajlliña Ajllt'am jisk' jach'a qilqanaka Yatiyaw apnaqaña: &Janitix qillt'atax katuyaski ukax uñacht'ayipana Yatiyaw uñachayiri Arupirw tuqit uñachayiri &Kunapachatix limt'añ limtáskix ukhakiw uñacht'äyi Tansa &Aru arsuña (%s bytes) apaqawix qalltaskiwa ... Machaq arux wasitat uǹacht'ayataniwa.
El nuevo idioma estará disponible al reiniciar el programa. <b>%s</b> arux janiw jikxataskiti.
La palabra no ha sido encontrada. Pacha suyt'a, janir achakun uñachawip imantkasina P'iqiñchawi Machaq arupiw achuntayañatakix intirnitatpach, <b>Arupirwanak apnaqaña...</b> ajllim. Ukat <b>Arupirwanak waraqaña</b> laphimanqhat mä arupirw ajllima.

Para instalar un diccionario nuevo desde el internet, seleccione <b>Arupirwanak apnaqaña...</b> y escoja un diccionario de la pestaña <b>Arupirwanak waraqaña</b>. Kiputpach machaq arupiw achuntayañatakixa, <b>Arupirwanaka</b> siski ukar saräma, ukatx <b>Kiputpach achuntayaña...</b> ajllima.
Para instalar un diccionario nuevo desde un archivo, seleccione <b>Kiputpach achuntayaña...</b> en el menú <b>Arupirwanaka</b>. Aru pirw apnaqañatakix maynir <b>Arupirwanaka</b> chhijllt'am.
Para empezar usando un diccionario, seleccione uno del menú de <b>Arupirwanaka</b>. Taqpach jach'a amuyunaka Qilqt'awi Babiloo yaqha &arur jaqukiptayaña &Babiloon aru jaqukipawipa... Qhana Kasta Janiw %s kipux apaqañjakiti Siqirata Pata &Laphi web Babiloor sart'aña 