# Khmer translation for babiloo
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the babiloo package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: babiloo\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2010-09-18 16:22+0100\n"
"PO-Revision-Date: 2009-12-26 14:53+0000\n"
"Last-Translator: chhorran <Unknown>\n"
"Language-Team: Khmer <km@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-09-21 18:28+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: ../qt/about_ui.py:119
msgid "About Babiloo"
msgstr "អំពី បេប៊ីលូ"

#: ../qt/about_ui.py:142
msgid "&About"
msgstr "អំពី"

#: ../qt/about_ui.py:150
msgid "A&uthors"
msgstr "អ្នក​និពន្ធ"

#: ../qt/about_ui.py:172
msgid "&License Agreement"
msgstr "កិច្ចព្រមព្រៀង អាជ្ញាបណ្ណ"

#: ../qt/about_ui.py:173
msgid "&Close"
msgstr "បិទ"

#: ../qt/main.py:144
msgid "Building main dialog..."
msgstr "កំពុងបង្កើត ផ្ទាំងសន្ទនាចំបង..."

#: ../qt/main.py:228
msgid "Help us with a donation"
msgstr ""

#: ../qt/main.py:244
msgid "Go to &previous article"
msgstr "ទៅ អត្ថបទមុន"

#: ../qt/main.py:249
msgid "Go to &next article"
msgstr "ទៅ អត្ថបទបន្ទាប់"

#: ../qt/main.py:254
msgid "&Save to file"
msgstr "រក្សាទុក ឯកសារ"

#: ../qt/main.py:259
msgid "Prin&t article"
msgstr "បោះពុម្ភ អត្ថបទ"

#: ../qt/main.py:264
msgid "Speak &word"
msgstr "អានពាក្យ"

#: ../qt/main.py:368
msgid "New Version Detected"
msgstr "បានប្រទះ កំណែថ្មី"

#: ../qt/main.py:369
#, python-format
msgid ""
"A new version of Babiloo has been released.\n"
"\n"
"New Version: %s\n"
"Current Version: %s\n"
"\n"
"Would you like to download the new version now?"
msgstr ""
"បានដាក់អោយប្រើប្រាស់ មួយកំណែថ្មី នៃបេប៊ីលូ។\n"
"កំណែថ្មី ៖ %s\n"
"កំណែ បច្ចុប្បន្ន ៖ %s\n"
"\n"
"អ្នកចង់ទាញយកឥឡូវ កំណែថ្មី ?"

#: ../qt/main.py:480 ../qt/main.py:489 ../qt/main.py:563
#: ../qt/onlineDictionariesView.py:280 ../qt/onlineDictionariesView.py:317
#: ../qt/onlineDictionariesView.py:323 ../qt/onlineDictionariesView.py:333
#: ../qt/onlineDictionariesView.py:350 ../qt/dictionariesview.py:100
#: ../qt/dictionariesview.py:146
msgid "Error"
msgstr "កំហុស"

#: ../qt/main.py:480
msgid "Please load a dictionary first."
msgstr "សូមផ្ទុកដំបូង មួយវចនានុក្រម។"

#: ../qt/main.py:489
#, python-format
msgid "Error while loading the word %s"
msgstr "កំហុស ពេលផ្ទុកពាក្យ %s"

#: ../qt/main.py:557
msgid "Import one or more dictionaries"
msgstr ""

#: ../qt/main.py:563
msgid ""
"Error trying to load the dictionary.\n"
"File corrupted or not supported."
msgstr ""
"កំហុសព្យាយាម ផ្ទុកវចនានុក្រម\n"
"ឯកសារខូច ឬ គ្មានត្រូវបានគាំទ្រ។"

#: ../qt/main.py:575
#, python-format
msgid "%s was not loaded: %s"
msgstr "%s គ្មានត្រូវបានផ្ទុក ៖ %s"

#: ../qt/main.py:587
msgid "Dictionary successfully installed."
msgstr ""

#: ../qt/main.py:589 ../qt/onlineDictionariesView.py:323
msgid "Dictionary selected couldn't be installed."
msgstr ""

#: ../qt/main.py:590 ../qt/onlineDictionariesView.py:354
#: ../qt/onlineDictionariesView.py:363 ../qt/dictionariesview.py:87
msgid "Info"
msgstr "ពត៌មាន"

#: ../qt/definitionView.py:44
msgid "Short Usage Information:"
msgstr "ពត៌មានប្រើប្រាស់ ខ្លី ៖"

#: ../qt/definitionView.py:45
msgid ""
"To start using dictionary, select one from the <b>Dictionaries</b> menu."
msgstr ""
"ដើម្បី ផ្តើមប្រើប្រាស់ វចនានុក្រម, ជ្រើសយក មួយ ពីមែនញាវ <b>វចនានុក្រម</b>។"

#: ../qt/definitionView.py:46
msgid ""
"To install a new dictionary from Internet, select <b>Manage "
"Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab."
msgstr ""
"ដើម្បីតំលើង មួយវចនានុក្រមថ្មី ពីអិនរើណែត, ជ្រើសយក <b>គ្រប់គ្រង "
"វចនានុក្រម...</b> និង ជ្រើសរើសមួយ ពី ផ្ទាំង <b>ទាញយក វចនានុក្រម</b>។"

#: ../qt/definitionView.py:47
msgid ""
"To install a new dictionary from file, select <b>Install from file...</b> "
"from the <b>Dictionaries</b> menu."
msgstr ""
"ដើម្បីតំលើង មួយវចនានុក្រមថ្មី ពីឯកសារ, ជ្រើសយក <b>តំលើង ពីឯកសារ...</b> ពី "
"មែនញាវ <b>វចនានុក្រម</b>។"

#: ../qt/definitionView.py:55
#, python-format
msgid "The word <b>%s</b> is not found."
msgstr "គ្មានរកឃើញ ពាក្យ <b>%s</b>។"

#: ../qt/settings_ui.py:511
msgid "Babiloo Settings"
msgstr "ការកំណត់ បេប៊ីលូ"

#: ../qt/settings_ui.py:512
msgid "Interface Language:"
msgstr "ភាសា ផ្ទាំងប្រទាក់"

#: ../qt/settings_ui.py:513
msgid "Translate this Application..."
msgstr "ប្រែសំរួល កម្មវិធីអនុវត្តនេះ..."

#: ../qt/settings_ui.py:514
msgid "Case sensitive"
msgstr "ប្រកាន់ អក្សរតូចធំ"

#: ../qt/settings_ui.py:515
msgid "AutoComplete"
msgstr "បំពេញស្វ័យប្រវត្តិ"

#: ../qt/settings_ui.py:516
msgid "Ignore accents"
msgstr "មិនខ្វល់ ការសង្កត់សំលេង"

#: ../qt/settings_ui.py:517
msgid "Pronounce words using this command:"
msgstr "អានពាក្យ ដោយប្រើប្រាស់ បញ្ជានេះ៖"

#: ../qt/settings_ui.py:518
msgid "Global settings"
msgstr "ការកំណត់ទូទៅ"

#: ../qt/settings_ui.py:521
msgid "Move up"
msgstr "ប្តូរទីតាំង ទៅលើ"

#: ../qt/settings_ui.py:522
msgid "Up"
msgstr "ទៅលើ"

#: ../qt/settings_ui.py:523
msgid "Move down"
msgstr "ប្តូរទីតាំង ទៅក្រោម"

#: ../qt/settings_ui.py:524
msgid "Down"
msgstr "ក្រោម"

#: ../qt/settings_ui.py:525 ../qt/settings_ui.py:538 ../qt/settings_ui.py:545
msgid "Show information about dictionary"
msgstr "បង្ហាញពត៌មាន អំពីវចនានុក្រម"

#: ../qt/settings_ui.py:526 ../qt/settings_ui.py:539 ../qt/settings_ui.py:546
msgid "Show info"
msgstr "បង្ហាញ ពត៌មាន"

#: ../qt/settings_ui.py:529
msgid "Directory storing the dictionaries:"
msgstr "ថតឯកសារ ផ្ទុកវចនានុក្រម ៖"

#: ../qt/settings_ui.py:530
msgid "Browse..."
msgstr "រាវរក"

#: ../qt/settings_ui.py:531
msgid "Installed Dictionaries"
msgstr "វចនានុក្រម ត្រូវបានតំលើង"

#: ../qt/settings_ui.py:532
msgid "From link:"
msgstr "ពី តំណភ្ជាប់"

#: ../qt/settings_ui.py:533 ../qt/settings_ui.py:534 ../qt/settings_ui.py:536
msgid "Download dictionary"
msgstr "ទាញយក វចនានុក្រម"

#: ../qt/settings_ui.py:535
msgid "From list:"
msgstr "ពី បញ្ជី ៖"

#: ../qt/settings_ui.py:537
msgid "Download"
msgstr "ទាញយក"

#: ../qt/settings_ui.py:541 ../qt/main_ui.py:284
msgid "Download Dictionaries"
msgstr "ទាញយក វចនានុក្រម"

#: ../qt/settings_ui.py:542 ../qt/main_ui.py:272
msgid "Dictionaries"
msgstr "វចនានុក្រម"

#: ../qt/settings_ui.py:548
msgid "Plugins"
msgstr "កូនកម្មវិធីភ្ជាប់"

#: ../qt/settings_ui.py:549
msgid "Behavior"
msgstr "ឫកពា"

#: ../qt/settings_ui.py:550
msgid "Pronounce the word"
msgstr "អានពាក្យ"

#: ../qt/settings_ui.py:551
msgid "Timeout before hide after mouse over"
msgstr "ផុតពេលកំណត់ មុនបិទបាំង បន្ទាប់ពីដាក់កណ្តុរលើ"

#: ../qt/settings_ui.py:552
msgid "Don't hide"
msgstr "កុំបិទបាំង"

#: ../qt/settings_ui.py:553
msgid " seconds"
msgstr " វិនាទី"

#: ../qt/settings_ui.py:554
msgid "Show only if modifier pressed"
msgstr "បង្ហាញ ត្រឹមបើ អ្នកផ្លាស់ប្តូរបានសង្កត់"

#: ../qt/settings_ui.py:559
msgid "Scan selection"
msgstr "ចាប់ ជំរើសយក"

#: ../qt/settings_ui.py:560
msgid "Show if word not found"
msgstr "បង្ហាញ បើគ្មានរកឃើញពាក្យ"

#: ../qt/settings_ui.py:561
msgid "Apperance"
msgstr "រាងរៅ"

#: ../qt/settings_ui.py:562
msgid "Transparency"
msgstr "ភាពថ្លា"

#: ../qt/settings_ui.py:564
msgid "Default width"
msgstr "ទទឹងលំនាំដើម"

#: ../qt/settings_ui.py:565
msgid "Default height"
msgstr "កំពស់ លំនាំដើម"

#: ../qt/settings_ui.py:566
msgid "Popup window"
msgstr "បង្អួចផុស"

#: ../qt/settings_ui.py:567
msgid "Preview"
msgstr "មើលមុន"

#: ../qt/settings_ui.py:568
msgid "Element"
msgstr "ធាតុ"

#: ../qt/settings_ui.py:569
msgid "Select element"
msgstr "ជ្រើសយក ធាតុ"

#: ../qt/settings_ui.py:570
msgid "Title"
msgstr "ចំណងជើង"

#: ../qt/settings_ui.py:571
msgid "Explanation"
msgstr "ការពន្យល់"

#: ../qt/settings_ui.py:572
msgid "Abbreviation"
msgstr "ពាក្យខ្លី"

#: ../qt/settings_ui.py:573
msgid "Example"
msgstr "ឧទាហរ"

#: ../qt/settings_ui.py:574
msgid "Transcription"
msgstr "របៀបអាន"

#: ../qt/settings_ui.py:575
msgid "Select font"
msgstr "ជ្រើសយក ក្រមអក្សរ"

#: ../qt/settings_ui.py:576
msgid "Foreground"
msgstr "ផ្ទៃ​ខាងមុខ"

#: ../qt/settings_ui.py:577
msgid "Background"
msgstr "ផ្ទៃ​ខា​ងក្រោយ"

#: ../qt/settings_ui.py:578 ../qt/settings_ui.py:580
msgid "Click to select color"
msgstr "ចុចជ្រើសយក ពណ៌"

#: ../qt/settings_ui.py:582
msgid "Select font size"
msgstr "ជ្រើសយក ទំហំក្រមអក្សរ"

#: ../qt/settings_ui.py:583
msgid "Bold"
msgstr "ដិត"

#: ../qt/settings_ui.py:585
msgid "Italic"
msgstr "ទ្រេត"

#: ../qt/settings_ui.py:587
msgid "Underline"
msgstr "គូស​បន្ទាត់​​​ក្រោម"

#: ../qt/settings_ui.py:589
msgid "Size"
msgstr "ទំហំ"

#: ../qt/settings_ui.py:590
msgid "Font"
msgstr "ក្រមអក្សរ"

#: ../qt/settings_ui.py:591
msgid "Appearance"
msgstr "រាងរៅ"

#: ../qt/settings_ui.py:592
msgid "Ok"
msgstr ""

#: ../qt/settings_ui.py:593
msgid "Cancel"
msgstr "បោះបង់"

#: ../qt/dictfilemanager.py:82
#, python-format
msgid "Loading %s..."
msgstr "កំពុងផ្ទុក​ %s..."

#: ../qt/main_ui.py:256 ../qt/main_ui.py:260
msgid "Clear the search box"
msgstr "សំអាត ប្រអប់ស្វែងរក"

#: ../qt/main_ui.py:257 ../qt/main_ui.py:261
msgid "Clear"
msgstr "លុបសំអាត"

#: ../qt/main_ui.py:258
msgid "Articles"
msgstr "អត្ថបទ"

#: ../qt/main_ui.py:259
msgid "History"
msgstr "ប្រវត្តិ"

#: ../qt/main_ui.py:263
msgid "Search"
msgstr "ស្វែងរក"

#: ../qt/main_ui.py:269
msgid "&File"
msgstr "ឯកសារ"

#: ../qt/main_ui.py:270
msgid "&Help"
msgstr "ជំនួយ"

#: ../qt/main_ui.py:271
msgid "&Settings"
msgstr "ការ​កំណត់"

#: ../qt/main_ui.py:273
msgid "&Quit"
msgstr "ឃ្លាត"

#: ../qt/main_ui.py:274
msgid "&About Babiloo"
msgstr "អំពី បេប៊ីលូ"

#: ../qt/main_ui.py:275
msgid "&Configure Babiloo"
msgstr "កំណត់ទំរង់ បេប៊ីលូ"

#: ../qt/main_ui.py:276
msgid "&Scan"
msgstr "ចាប់"

#: ../qt/main_ui.py:277
msgid "Install from file..."
msgstr "តំលើង ពី ឯកសារ..."

#: ../qt/main_ui.py:278 ../qt/main_ui.py:279
msgid "Import"
msgstr "នាំចូល"

#: ../qt/main_ui.py:280
msgid "Manage Dictionaries..."
msgstr "គ្រប់គ្រង វចនានុក្រម..."

#: ../qt/main_ui.py:281
msgid "Visit HomePage"
msgstr "ចូលមើល ទំព័រដើម"

#: ../qt/main_ui.py:282
msgid "Report A Problem"
msgstr "រាយការ មួយបញ្ហា"

#: ../qt/main_ui.py:283
msgid "Translate This Application..."
msgstr "ប្រែសំរួល កម្មវិធីអនុវត្តនេះ..."

#: ../qt/onlineDictionariesView.py:235
msgid "Downloading files..."
msgstr "កំពុងទាញយក ឯកសារ..."

#: ../qt/onlineDictionariesView.py:235 ../qt/onlineDictionariesView.py:362
#: ../qt/dictionariesview.py:86
msgid "&Abort"
msgstr "បោះចោល"

#: ../qt/onlineDictionariesView.py:236
msgid "Downloading"
msgstr "ការទាញយក"

#: ../qt/onlineDictionariesView.py:249
#, python-format
msgid "Starting the download (%s bytes) ..."
msgstr "កំពុងផ្តើម ការទាញយក (%s បៃ) ..."

#: ../qt/onlineDictionariesView.py:280
#, python-format
msgid "Unable to download file %s"
msgstr "គ្មានអាចទាញយក ឯកសារ %s"

#: ../qt/onlineDictionariesView.py:317
msgid "Link address is empty"
msgstr "អាសយដ្ឋានតំណភ្ជាប់ ទទេ"

#: ../qt/onlineDictionariesView.py:350
#, python-format
msgid ""
"Dictionary that couldn't be installed:\n"
" %s"
msgstr ""
"វចនានុក្រម ដែលគ្មានអាច ត្រូវបានទាញយក ៖\n"
"%s"

#: ../qt/onlineDictionariesView.py:362 ../qt/dictionariesview.py:86
msgid "Rescanning dictionaries..."
msgstr "កំពុងបបោសរក វចនានុក្រម..."

#: ../qt/settings.py:82
msgid "Alert"
msgstr "ដាស់តឿន"

#: ../qt/settings.py:82
msgid "The new language will be displayed after restarting the program."
msgstr "ភាសាថ្មី នឹងត្រូវបានបង្ហាញចេញ បន្ទាប់ពីផ្តើមឡើងវិញ កម្មវិធី។"

#: ../qt/settings.py:138
msgid "Open Directory"
msgstr "បើក ថតឯកសារ"

#: ../qt/dictionariesview.py:31
msgid "Type"
msgstr "ប្រភេទ"

#: ../qt/dictionariesview.py:31
msgid "Dictionary name"
msgstr "ឈ្មោះវចនានុក្រម"

#: ../qt/dictionariesview.py:31
msgid "Total articles"
msgstr "អត្ថបទ សរុប"

#: ../qt/dictionariesview.py:143
msgid "Export dictionary as..."
msgstr "នាំចេញ វចនានុក្រម ជា..."

#: ../core/languages/Languages.py:28
msgid "Albanian"
msgstr ""

#: ../core/languages/Languages.py:29
msgid "Arabic"
msgstr ""

#: ../core/languages/Languages.py:30
msgid "Armenian"
msgstr ""

#: ../core/languages/Languages.py:31
msgid "Malay"
msgstr ""

#: ../core/languages/Languages.py:32
msgid "Bosnian"
msgstr ""

#: ../core/languages/Languages.py:33
msgid "Brazilian"
msgstr ""

#: ../core/languages/Languages.py:34
msgid "Bulgarian"
msgstr ""

#: ../core/languages/Languages.py:35
msgid "Catalan"
msgstr ""

#: ../core/languages/Languages.py:36
msgid "Basque"
msgstr ""

#: ../core/languages/Languages.py:37
msgid "Chinese"
msgstr ""

#: ../core/languages/Languages.py:38
msgid "Croatian"
msgstr ""

#: ../core/languages/Languages.py:39
msgid "Czech"
msgstr ""

#: ../core/languages/Languages.py:40
msgid "Danish"
msgstr ""

#: ../core/languages/Languages.py:41
msgid "Dutch"
msgstr ""

#: ../core/languages/Languages.py:42
msgid "English"
msgstr ""

#: ../core/languages/Languages.py:43
msgid "British English"
msgstr ""

#: ../core/languages/Languages.py:44
msgid "Esperanto"
msgstr ""

#: ../core/languages/Languages.py:45
msgid "Estonian"
msgstr ""

#: ../core/languages/Languages.py:46
msgid "Finnish"
msgstr ""

#: ../core/languages/Languages.py:47
msgid "French"
msgstr ""

#: ../core/languages/Languages.py:48
msgid "Galician"
msgstr ""

#: ../core/languages/Languages.py:49
msgid "Georgian"
msgstr ""

#: ../core/languages/Languages.py:50
msgid "German"
msgstr ""

#: ../core/languages/Languages.py:51
msgid "Greek"
msgstr ""

#: ../core/languages/Languages.py:52
msgid "Hebrew"
msgstr ""

#: ../core/languages/Languages.py:53
msgid "Hungarian"
msgstr ""

#: ../core/languages/Languages.py:54
msgid "Indonesian"
msgstr ""

#: ../core/languages/Languages.py:55
msgid "Italian"
msgstr ""

#: ../core/languages/Languages.py:56
msgid "Japanese"
msgstr ""

#: ../core/languages/Languages.py:57
msgid "Kazakh"
msgstr ""

#: ../core/languages/Languages.py:58
msgid "Korean"
msgstr ""

#: ../core/languages/Languages.py:59
msgid "Latvian"
msgstr ""

#: ../core/languages/Languages.py:60
msgid "Lithuanian"
msgstr ""

#: ../core/languages/Languages.py:61
msgid "Luxembourgish"
msgstr ""

#: ../core/languages/Languages.py:62
msgid "Macedonian"
msgstr ""

#: ../core/languages/Languages.py:63
msgid "Norwegian"
msgstr ""

#: ../core/languages/Languages.py:64
msgid "Persian"
msgstr ""

#: ../core/languages/Languages.py:65
msgid "Polish"
msgstr ""

#: ../core/languages/Languages.py:66
msgid "Portuguese"
msgstr ""

#: ../core/languages/Languages.py:67
msgid "Romanian"
msgstr ""

#: ../core/languages/Languages.py:68
msgid "Russian"
msgstr ""

#: ../core/languages/Languages.py:69
msgid "Serbian"
msgstr ""

#: ../core/languages/Languages.py:70
msgid "Slovak"
msgstr ""

#: ../core/languages/Languages.py:71
msgid "Slovenian"
msgstr ""

#: ../core/languages/Languages.py:72
msgid "Spanish"
msgstr ""

#: ../core/languages/Languages.py:73
msgid "Swedish"
msgstr ""

#: ../core/languages/Languages.py:74
msgid "Thai"
msgstr ""

#: ../core/languages/Languages.py:75
msgid "Turkish"
msgstr ""

#: ../core/languages/Languages.py:76
msgid "Ukrainian"
msgstr ""

#: ../core/languages/Languages.py:77
msgid "Vietnamese"
msgstr ""
