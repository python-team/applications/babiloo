��    t      �  �   \      �	     �	     �	     �	     �	     �	     
     
     '
     -
     3
     F
     L
     Z
  	   `
  �   j
     �
     �
            
     	   !     +     4     A  
   R     ]     f  	   k     u     �     �     �     �     �     �     �     �     �  *     #   9  
   ]     h     m     v     �     �     �     �     �  E   �          5     =     I     a  
   f  
   q  
   |     �     �     �     �     �     �     �     �     �          &     -     C     Q  	   h     r     z     �     �     �     �     �     �     �  #   �     !     2     M     \     c     r     ~     �     �  	   �  !   �     �     	       $     @   ?      �  $   �     �  �   �  l   V  H   �               )     G     e     r     w  	   �     �     �  �  �  	   F     P  	   f  
   p     {     �     �     �     �     �     �     �  	   �     �  �        �     �     �     �  
   �  
   �  
   �     �     �                    #  "   /     R  "   [     ~  �   �  !         B     X     m     z  0   �  -   �  
   �     �  	             "     8     D     `     i  F   o  '   �     �     �     �               +     <     L     f     �  	   �     �     �     �     �     �     �       &        ;     J     f     l     r     �  (   �     �     �     �     �       '        B     X     u     �     �     �     �     �  &   �       )   %  /   O          �  $   �  <   �  +   �  8   )     b  �   j  ~     T   �     �     �     �          7     E  #   J  	   n     x  !        g   '          F   K          f              ^          0   U   T   D       H   ?       I       C      P   8       G                      J   m   h              o            A       Z   	       Y   M   )          ,   R   s   k          5            <   >   n           j   L   *   .       r           #   
      7   2      c   \      4       !             (   i   6              t   -      S   p           a       "   V       &       :       d   N   %       =          9      /              ;   b   X   O   [   _   W      l   +   3           E   Q   `         e       1   q   ]   B   @   $         seconds %s was not loaded: %s &Abort &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings A new version of Babiloo has been released.

New Version: %s
Current Version: %s

Would you like to download the new version now? A&uthors Abbreviation About Babiloo Alert Appearance Apperance Articles AutoComplete Babiloo Settings Background Behavior Bold Browse... Building main dialog... Cancel Case sensitive Clear Clear the search box Click to select color Default height Default width Dictionaries Dictionary name Dictionary that couldn't be installed:
 %s Directory storing the dictionaries: Don't hide Down Download Download Dictionaries Download dictionary Downloading Downloading files... Element Error Error trying to load the dictionary.
File corrupted or not supported. Error while loading the word %s Example Explanation Export dictionary as... Font Foreground From link: From list: Global settings Go to &next article Go to &previous article History Ignore accents Import Info Install from file... Installed Dictionaries Interface Language: Italic Link address is empty Loading %s... Manage Dictionaries... Move down Move up New Version Detected Open Directory Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Rescanning dictionaries... Scan selection Search Select element Select font Select font size Short Usage Information: Show if word not found Show info Show information about dictionary Show only if modifier pressed Size Speak &word Starting the download (%s bytes) ... The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Transcription Translate This Application... Translate this Application... Transparency Type Unable to download file %s Underline Up Visit HomePage Project-Id-Version: babiloo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-18 16:22+0100
PO-Revision-Date: 2010-07-18 20:41+0000
Last-Translator: lordcaos777 <Unknown>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-21 18:28+0000
X-Generator: Launchpad (build Unknown)
  segundos %s no fue cargado: %s C&ancelar &Acerca de &Acerca de Babiloo &Cerrar &Configurar Babiloo &Archivo Ay&uda &Acuerdo de licencia &Salir &Guardar al archivo &Explorar &Configuración Una nueva versión de Babiloo ha salido a la luz.

Versión nueva: %s
Versión actual: %s

Le gustaría descargar la nueva versión ahora? A&utores Abreviación Acerca de Babiloo Aviso Apariencia Apariencia Artículos AutoCompletar Configuración de Babiloo Fondo Comportamiento Negrita Examinar... Construyendo diálogo principal... Cancelar Distinguir mayusculas y minusculas Borrar This requirement definition document defines the requirements from product development area specified by General Insurance product management in Partenon. Hacer clic para seleccionar color Altura predeterminada Ancho predeterminado Diccionarios Nombre del diccionario Diccionarios que no pudieron ser instalados:
 %s Directorio donde se guardan los diccionarios: No ocultar Abajo Descargar Descargar diccionarios Descargar diccionario Descargando Descargando los archivos... Elemento Error Error intentando cargar el diccionario.
Archivo corrupto o no válido. Error mientras se cargaba la palabra %s Ejemplo Explicación Exportar diccionario como... Tipo de letra Primer plano Desde un enlace: Desde la lista: Configuraciones Generales Ir al artículo &siguiente Ir al artículo &anterior Historial Ignorar acentos Importar Info Instalar desde archivo... Diccionarios instalados Idioma de la interfaz: Cursiva La dirección del enlace está vacía. Cargando %s... Administrar diccionarios... Bajar Subir Nueva Versión Detectada Abrir Directorio Por favor primero cargue un diccionario. Extensiones Ventana emergente Vista previa &Imprimir artículo Pronunciar la palabra Pronunciar palabras usando este comando Comunicar un problema Reescaneando diccionarios... Escanear la selección Buscar Seleccionar elemento Seleccionar fuente Seleccione el tamaño de fuente Instrucciones de uso: Mostrar si la palabra no es encontrada Mostrar info Mostrar información sobre el diccionario Mostrar solo si el modificador está presionado Tamaño Pronunciar &palabra Empezando la descarga (%s bytes) ... El nuevo idioma estará disponible al reiniciar el programa. La palabra <b>%s</b> no ha sido encontrada. Tiempo de espera antes de esconder el puntero del ratón Título Para instalar un diccionario nuevo desde el internet, seleccione <b>Organizar Diccionarios...</b> y escoja uno de la pestaña <b>Descargar Diccionarios</b>. Para instalar un diccionario nuevo desde un archivo, seleccione <b>Instalar de archivo...</b> en el menú <b>Diccionarios</b>. Para empezar usando un diccionario, seleccione uno del menú de <b>Diccionarios</b>. Artículos totales Transcripción Traducir Esta Aplicación... Traducir esta aplicación... Transparencia Tipo No se puede descargar el fichero %s Subrayado Arriba Visitar la página web de Babiloo 