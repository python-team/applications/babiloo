��    l      |  �   �      0	     1	     :	     B	     D	     K	     Z	     a	     t	     z	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     �	  
   �	  	   �	     �	     
     
     
     
  
   )
     4
     =
     B
     Z
     i
     o
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
  
   �
     	          $     ,  E   2     x     �     �     �  
   �     �     �     �     �     �     �          !     &     ;     R     f     m     {  	   �     �     �     �     �     �     �     �  #        '     8     G     N     ]     i     z     �     �  	   �  !   �     �     �            @   )      j  $   �     �  �   �  l   @  H   �     �          #     0     5  	   7     A     D     S  �  W               !  
   #     .  
   A     L  
   _  
   j     u  
   �     �  
   �     �     �  
   �     �     �     �     �     �     �     �                         #     *     1     G     W     ^     n     ~     �     �     �     �     �     �     �  	   �     �     �     �     �  O   �     H     O     V     l     s     z     �     �     �     �     �     �     �     �     �                    -     =     D     K     g     n     {     �     �     �     �     �     �     �     �     �     
               7     D     W     j     �     �  '   �     �     �     �  b   �  e   ^  7   �     �     	  	        &     -  	   /     9     =     J     f       :   L   
                _          *   8   %      F   I              h   @   d   e   c           C   B       '       )   3   ?              J   g             M   (                                #       U   b                  &       4      ^   >   $   6   2                    N   V          -               Z   W   9       5       i   P      \      k              O       R       ]   "       T   E   H   +          D   .   =      7          `   /       Y   !       [   	   l   G      A       1   S      <   K      Q   j   ,   X   0           a   ;        seconds #000000 % &About &About Babiloo &Close &Configure Babiloo &File &Help &License Agreement &Quit &Save to file &Scan &Settings ... A&uthors Abbreviation About Babiloo Alt Appearance Apperance Articles AutoComplete B Babiloo Babiloo Settings Background Behavior Bold Building main dialog... Case sensitive Clear Clear the search box Click to select color Configure Configure plugin Control Ctrl+Q Default height Default width Dictionaries Dictionary name Don't hide Down Download Dictionaries Element Error Error trying to load the dictionary.
File corrupted or not supported. Example Explanation Export dictionary as... Font Foreground Global settings Go to &next article Go to &previous article History I Import Import one or many dictionaries Info Install from file... Installed Dictionaries Interface Language: Italic Loading %s... Manage Dictionaries... Move down Move up Please load a dictionary first. Plugins Popup window Preview Prin&t article Pronounce the word Pronounce words using this command: Report A Problem Scan selection Search Select element Select font Select font size Shift Short Usage Information: Show if word not found Show info Show information about dictionary Show information about plugin Show only if modifier pressed Size Speak &word The new language will be displayed after restarting the program. The word <b>%s</b> is not found. Timeout before hide after mouse over Title To install a new dictionary from Internet, select <b>Manage Dictionaries...</b> and choose one from the <b>Download Dictionaries</b> tab. To install a new dictionary from file, select <b>Install from file...</b> from the <b>Dictionaries</b> menu. To start using dictionary, select one from the <b>Dictionaries</b> menu. Total articles Translate This Application... Transparency Type U Underline Up Visit HomePage Win Project-Id-Version: Babiloo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-01-22 14:30+0100
PO-Revision-Date: 2009-03-24 17:17+0800
Last-Translator: Grissiom <chaos.proton@gmail.com>
Language-Team: Grissiom <chaos.proton@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2009-02-08 12:44+0000
X-Generator: Launchpad (build Unknown)
  秒 #000000 % 关于(&A) 关于 Babiloo(&A) 关闭(&C) 设置 Babiloo(&C) 文件(&F) 帮助(&H) 授权协议(&L) 离开(&Q) 保存到文件(&S) 取词(&S) &设置 ... 作者(&u) 缩写 关于 Babiloo Alt 外观 外观 条目 自动补全 B Babiloo Babiloo 设置 背景 行为 黑体 构建主对话框... 区分大小写 清除 清除搜索框 点击以选择 设定 设定插件 Control Ctrl+Q 默认高度 默认宽度 字典 字典名称 不隐藏 下 下载字典 元素 错误 载入字典时发生错误。
文件已损坏或者属于不支持的类型。 例子 解释 将字典导出为... 字体 前景 全局设置 下一个条目(&n) 上一个条目(&p) 历史 I 导入 载入一个或多个字典 信息 从文件安装... 已安装的字典 界面语言: 斜体 载入中 %s... 管理字典... 下移 上移 请先载入一个字典。 插件 弹出窗口 预览 打印条目(&t) 单词发音 使用如下命令发音 报告错误 选择区取词 搜索 选择元素 选择字体 选择字体大小 Shift 用法简介: 当未找到词时显示 显示信息 显示字典信息 显示插件信息 显示，仅当按下 大小 读出单词 (&w) 新语言将在下次启动时显示。 找不到单词 <b>%s</b> 鼠标悬停隐藏超时 标题 想从互联网上安装一个新字典，请在<b>字典</b>菜单里选择<b>下载字典</b>。 想从文件中安装一个字典，请选择在<b>字典</b>菜单里选择<b>从文件安装...</b> 想启用字典，请在<b>字典</b>菜单里选择。 条目总数 翻译这个软件 透明度 类型 U 下划线 上 访问主页 Win 